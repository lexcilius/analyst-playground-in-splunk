#!/usr/bin/env bash

exe () {
    MESSAGE_PREFIX="\b\b\b\b\b\b\b\b\b\b"
    echo -e "$MESSAGE_PREFIX Execute: $1"
    LOOP=0
    while true;
    do
        if ! [ $LOOP == 0 ]; then echo -e "$MESSAGE_PREFIX ...     "; fi;
        sleep 90;
        LOOP=$((LOOP+1))
    done & ERROR=$("${@:2}" 2>&1)
    status=$?
    kill $!; trap 'kill $!' SIGTERM

    if [ $status -ne 0 ];
    then
        echo -e "$MESSAGE_PREFIX ✖ Error" >&2
        echo -e "$ERROR" >&2
    else
        echo -e "$MESSAGE_PREFIX ✔ Success"
    fi
    return $status
}

splunk_download () {
    # Create download dir for Splunk if not exists
    mkdir -p "${filelocation}"

    # Check cache
    curl -so "${filelocation}/${md5File}" "${url}/${md5File}"
    md5=$(cat ${filelocation}/${md5File} | cut -f4 -d\ )

    # Do we have hash match?
    if [ -s ${filelocation}/${filename} ]; then
      md5cache=$(md5sum ${filelocation}/${filename} | cut -f1 -d\ )
      if [ "${md5}" = "${md5cache}" ]; then
        # Package already downloaded
        download=0
      fi
    fi

    if [ ${download} -eq 1 ]; then
      # Downloading Splunk package
      curl -so "${filelocation}/${filename}" "${url}/${filename}"
    fi
}

bots_v1_download () {

    # BOTS 1.0 Complete dataset (6.1GB)
    # curl --silent --url https://s3.amazonaws.com/botsdataset/botsv1/splunk-pre-indexed/botsv1_data_set.tgz --output /vagrant/provisioning/apps/bots-v1-all.tgz
    # BOTS 1.0 Only Attack Data (135MB)
    curl --silent --url https://s3.amazonaws.com/botsdataset/botsv1/botsv1-attack-only.tgz --output /vagrant/provisioning/apps/bots-v1.tgz
}

bots_v2_download () {

    # BOTS 2.0 Complete dataset (16.4GB)
    # curl --silent --url https://s3.amazonaws.com/botsdataset/botsv2/botsv2_data_set.tgz --output /vagrant/provisioning/apps/bots-v2-all.tgz
    # BOTS 2.0 Only Attack Dataset (3.2GB)
    curl --silent --url https://s3.amazonaws.com/botsdataset/botsv2/botsv2_data_set_attack_only.tgz --output /vagrant/provisioning/apps/bots-v2.tgz
}

packages_install () {
    sudo yum install -y wireshark patch git
}

splunk_install () {
    # Install Splunk and it's dependencies (none at the moment, but yum should be able to solve it)
    sudo yum install -y "${filelocation}/${filename}"
}

splunk_apps_v1_install () {
    # Change owner in Apps dir
    sudo chown -R ${splunk_system_user}:${splunk_system_user} ${splunk_home}/etc/apps

    # Install all the apps we have prepared
    # TODO: I should use the same find/search for patches & configs
    for app in /vagrant/provisioning/apps/${bots_v1_apps}.tgz; do
      sudo tar --warning=no-unknown-keyword --owner=splunk -xzf ${app} -C ${splunk_home}/etc/apps/
      exe "${app} installed"
    done

    # Some apps may have wrong permissions, fix the owners at least
    # sudo chown -R ${splunk_system_user}:${splunk_system_user} ${splunk_home}/etc/apps/
}

splunk_apps_v2_install () {
    # Change owner in Apps dir
    sudo chown -R ${splunk_system_user}:${splunk_system_user} ${splunk_home}/etc/apps

    # Install all the apps we have prepared
    # TODO: I should use the same find/search for patches & configs
    for app in /vagrant/provisioning/apps/${bots_v2_apps}.tgz; do
      sudo tar --warning=no-unknown-keyword --owner=splunk -xzf ${app} -C ${splunk_home}/etc/apps/
      exe "${app} installed"
    done

    # Some apps may have wrong permissions, fix the owners at least
    # sudo chown -R ${splunk_system_user}:${splunk_system_user} ${splunk_home}/etc/apps/
}

config_splunk () {
    # Install the root admin credentials.
    sudo install -o ${splunk_system_user} -g ${splunk_system_user} -m 644 /vagrant/provisioning/user-seed.conf ${splunk_home}/etc/system/local

    # Copy the local configs there
    sudo install -o ${splunk_system_user} -g ${splunk_system_user} -m 644 /vagrant/provisioning/transactiontypes.conf ${splunk_home}/etc/system/local
    sudo install -o ${splunk_system_user} -g ${splunk_system_user} -m 644 /vagrant/provisioning/default.xml ${splunk_home}/etc/apps/investigate_workshop/default/data/ui/nav/default.xml
    sudo install -o ${splunk_system_user} -g ${splunk_system_user} -m 644 /vagrant/provisioning/apt-questions.xml ${splunk_home}/etc/apps/investigate_workshop/default/data/ui/views/apt-questions.xml
    sudo install -o ${splunk_system_user} -g ${splunk_system_user} -m 644 /vagrant/provisioning/ransomware-questions.xml ${splunk_home}/etc/apps/investigate_workshop/default/data/ui/views/ransomware-questions.xml

    # Fix some permissions, so that Splunk can operate smoothly
    sudo chown -R ${splunk_system_user}:${splunk_system_user} /opt/splunk/etc/apps/
    #sudo chown -R ${splunk_system_user}:${splunk_system_user} /vagrant/data/
    #sudo chmod -R og+rX /vagrant/data
}

init_splunk () {
    # Let Splunk create the init files
    sudo -H -u ${splunk_system_user} /opt/splunk/bin/splunk start --accept-license --answer-yes --no-prompt
    sudo /opt/splunk/bin/splunk enable boot-start -user ${splunk_system_user}

    # Adding the default user with can_delete and admin roles
    sudo -u ${splunk_system_user} -g ${splunk_system_user} /opt/splunk/bin/splunk add user ${splunk_user} -password ${splunk_user_password} -role admin -role can_delete -auth admin:changeme

    # We are pre-defining some user-settings
    sudo install -D -o ${splunk_system_user} -g ${splunk_system_user} -m 644 /vagrant/provisioning/user-prefs.conf ${splunk_home}/etc/users/${splunk_user}/user-prefs/local/user-prefs.conf
}

fixes () {
    # And fix the permissions. Interesting, I thought install would do this
    sudo chown -R ${splunk_system_user}:${splunk_system_user} ${splunk_home}/etc/users/${splunk_user}
}

splunk_restart () {
    # Run the service
    sudo service splunk restart
}

cleanup () {
    # Cleanup downloaded files
    rm "${filelocation}/${md5File}" && rm "${filelocation}/${filename}"
}

completed () {
    sleep 1
}

# Set the required verions & metadata
product="splunk"       # values can be : splunk , universalforwarder
version="7.3.0"        # Splunk product Version
hash="657388c7a488"     # specific per Version
arch="x86_64"          # values can be : x86_64 (redhat, tgz), amd64 (ubuntu), x64 (Windows)
os="linux"             # values can be : linux, windows
pkg="rpm"              # Values can be : tgz, rpm, deb, msi

splunk_home="/opt/splunk"

# Passing BOTS version into variable
splunk_bots_v="$1"

# OS user for running splunk
splunk_system_user="splunk"
# UI user
splunk_user="splunk"
splunk_user_password="splunk123"
# Splunk Admin user
splunk_admin="admin"
splunk_admin_password="changeme"

bots_v1_apps=(
     "fortinet-fortigate-add-on-for-splunk_160"
     "splunk-add-on-for-microsoft-windows_600"
     "add-on-for-microsoft-sysmon:810"
     "splunk-add-on-for-tenable_514"
     "splunk-stream_713"
     "splunk-ta-for-suricata_233"
     "boss-of-the-soc-bots-investigation-workshop-for-splunk_122"
     "url-toolbox_16"
     "threathunting_11"
     "punchcard-custom-visualization_130"
     "force-directed-app-for-splunk_200"
     "sankey-diagram-custom-visualization_130"
     "lookup-file-editor_331"
)

bots_v2_apps=(
    "add-on-for-microsoft-sysmon_810"
    "base64_1.1"
    "jellyfisher_010"
    "palo-alto-networks-add-on-for-splunk_611"
    "sa-investigator-for-enterprise-security_200"
    "splunk-add-on-for-apache-web-server_100"
    "splunk-add-on-for-microsoft-cloud-services_310"
    "splunk-add-on-for-microsoft-iss_101"
    "splunk-add-on-for-microsoft-windows_600"
    "splunk-add-on-for-symantec-endpoint-protection_230"
    "splunk-add-on-for-unix-and-linux_602"
    "splunk-app-for-osquery_10"
    "splunk-common-information-model-cim_4130"
    "splunk-security-essentials_242"
    "splunk-stream_713"
    "ssl-certificate-checker_32"
    "website-monitoring_274"
)

# Choose the right filename, based on the variables above
if [ $pkg = "tgz" ]; then
   filename="${product}-${version}-${hash}-Linux-${arch}.${pkg}"
elif [ $os = "windows" ]; then
   filename="${product}-${version}-${hash}-${arch}-release.${pkg}"
else
   filename="${product}-${version}-${hash}-${os}-2.6-${arch}.${pkg}"
fi

md5File="${filename}.md5"
url="https://download.splunk.com/products/splunk/releases/${version}/${os}/"
filelocation="/vagrant/splunk"
download=1

## Splunk Enterprise Provisioning Steps

exe 'Download Splunk Enterprise' splunk_download

if [ "$splunk_bots_v" = "v1" ]; then

  exe 'Download BOTS v1 dataset' bots_v1_download

  exe 'Install required packages' packages_install

  exe 'Install Splunk Enterprise' splunk_install

  exe 'Install Splunk Apps for BOTS v1' splunk_apps_v1_install


else if [ "$splunk_bots_v" = "v2" ]; then

    exe 'Download BOTS v2 dataset' bots_v2_download

    exe 'Install required packages' packages_install

    exe 'Install Splunk Enterprise' splunk_install

    exe 'Install Splunk Apps for BOTS v2' splunk_apps_v2_install

    else exit
    fi
fi

exe 'Configuring Splunk Enterprise' config_splunk

exe 'Init Splunk Enterprise' init_splunk

exe 'Fix permissions' fixes

exe 'Restarting Splunk Enterprise' splunk_restart

exe 'Clean Up' cleanup

echo -e "********************************************************************************"
echo -e "| Congratulations! Your Splunk instance is running at http://localhost:8080/   |"
echo -e "| Authenticate with ${splunk_user}:${splunk_user_password} (username:password)                       |"
echo -e "********************************************************************************"

exe 'Splunk Deployment completed' completed
