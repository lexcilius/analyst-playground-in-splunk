<dashboard>
  <label>Scenario #2 Questions: Ransomware</label>
  <row>
    <panel>
      <html tokens="true">
      <h2>
        <b>Questions</b>
      </h2>
      <h3>Scenario #2: Randomware</h3>
      <ul>
        <li><p>1) What was the most likely IP address of we8105desk on 24AUG2016?</p>
        <details>
          <summary>Hint #1</summary>
            <p>Keep it simple and just search for the hostname provided in the question.  Try using the stats command to get a count of events by source ip address to point you in the right direction.</p>
        </details>
        </li>
        <br/>
        <li><p>2) Amongst the Suricata signatures that detected the Cerber malware, which one alerted the fewest number of times? Submit ONLY the signature ID value as the answer. (No punctuation, just 7 integers.)
        </p>
        <details>
          <summary>Hint #1</summary>
            <p>Keep it simple and start your search by looking at only the sourcetype associated with Suricata and maybe even the name of the malware in question.  The field containing the signature ID should be obvious.  Use stats to create a count by the field containing the signature ID.</p>
        </details>
        </li>
        <br/>
        <li><p>3) What fully qualified domain name (FQDN) does the Cerber ransomware attempt to direct the user to at the end of its encryption phase?</p>
        <details>
          <summary>Hint #1</summary>
            <p>Search stream:dns data for A queries coming from the infected workstation IP on the date in question.  Try and narrow your search period.</p>
        </details>
        <details>
        <summary>Hint #2</summary>
          <p>Perform a shannon entropy analysis on the query{} field using URL toolbox by adding this to the end of the search: &#8739;`ut_shannon(query&#123;&#125;)` &#8739; stats count by ut_shannon, query&#123;&#125; &#8739; sort -ut_shannon </p>
        </details>
        </li>
        <br/>
        <li><p>4) What was the first suspicious domain visited by we8105desk on 24AUG2016?</p>
        <details>
          <summary>Hint #1</summary>
            <p>Search stream:dns data for A queries coming from the infected workstation IP on the date in question.</p>
        </details>
        <details>
          <summary>Hint #2</summary>
            <p>Use the "&#8739; reverse" SPL command to show oldest events first.</p>
        </details>
        <details>
          <summary>Hint #3</summary>
            <p>Eliminate domain lookups that you can explain, question the first one you cannot.</p>
        </details>
        <details>
          <summary>Hint #4</summary>
            <p>Go and get some IOCs on Cerber.  Then compare to the DNS Data</p>
        </details>
        </li>
        <br/>
        <li><p>5) During the initial Cerber infection a VB script is run. The entire script from this execution, pre-pended by the name of the launching .exe, can be found in a field in Splunk. What is the length in characters of the value of this field?</p>
        <details>
          <summary>Hint #1</summary>
            <p>Keep it simple.  Start by looking at sysmon data for the infected device on the date in question.  Calculate the length of the command line using the "len()" function of the "eval" SPL command, and give your eyes a break by using the splunk table command. </p>
        </details>
        </li>
        <br/>
        <li><p>6) What is the name of the USB key inserted by Bob Smith?</p>
        <details>
          <summary>Hint #1</summary>
            <p>Tough question. Perhaps you should give a try <a href="http://answers.splunk.com" target="_blank">here</a></p>
        </details>
        </li>
        <br/>
        <li><p>7) Bob Smith's workstation (we8105desk) was connected to a file server during the ransomware outbreak. What is the IP address of the file server?</p>
        <details>
          <summary>Hint #1</summary>
            <p>Search for SMB (Windows file sharing protocol) traffic from the infected device on the date in question. The "stats" SPL command can be used to count the most common destination IP for the SMB protocol.</p>
        </details>
        </li>
        <br/>
        <li><p>8) How many distinct PDFs did the ransomware encrypt on the remote file server? </p>
        <details>
          <summary>Hint #1</summary>
            <p>Don't use SMB this time - it's a trap!  Windows event logs are the way to go for this one.  Focus on the event types that deal with windows shares and narrow the search by looking for distinct filenames for the extension in question.</p>
        </details>
        </li>
        <br/>
        <li><p>9) The VBscript found in question 204 launches 121214.tmp. What is the ParentProcessId of this initial launch?</p>
        <details>
          <summary>Hint #1</summary>
            <p>Embrace your sysmon data. Search for a command issued by the infected device on the date in question referencing the filename in question, and use the process_id, ParentProcessId, CommandLine, and ParentCommandLine, to track down the parent process id of them all.</p>
        </details>
        </li>
        <br/>
        <li><p>10) The Cerber ransomware encrypts files located in Bob Smith's Windows profile. How many .txt files does it encrypt?</p>
        <details>
          <summary>Hint #1</summary>
            <p>Sysmon to the rescue again. Focus on the infected machine as well as the user profile while searching for the filename extension in question.</p>
        </details>
        <details>
          <summary>Hint #2</summary>
            <p>In Sysmon events, EventCode=2 indicates file creation time has changed. Watch out for duplicates!</p>
        </details>
        </li>
        <br/>
        <li><p>11) The malware downloads a file that contains the Cerber ransomware cryptor code. What is the name of that file? </p>
        <details>
          <summary>Hint #1</summary>
            <p>When looking for potentially malicious file, start your search with the Suricata data. Narrow your search by focusing on the infected device. Remember malware does not always have to begin as an executable file.</p>
        </details>
        </li>
        <br/>
        <li><p>12) Now that you know the name of the ransomware's encryptor file, what obfuscation technique does it likely use?</p>
        <details>
          <summary>Hint #1</summary>
            <p>The enrcyptor file was an image!</p>
        </details>
        </li>
      </ul>
    </html>
    </panel>
  </row>
  <row>
    <panel>
      <html tokens="true">
      <h2>
        <b>Navigation</b>
      </h2>
      <p>
          <a href="/app/investigate_workshop/introduction">Next: Introduction</a>
        </p>
    </html>
    </panel>
  </row>
</dashboard>
